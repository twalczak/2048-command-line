/*
    Project:    2048 
    Author:     Tomasz Walczak
    File:       2048.cpp
    Date:       2014-May-05
*/

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include "arena.h"
#include "logic.h"

//------------------------------------------------------------------------------------------// 2048 Main Entry
int main(void) {
    int i, j;
    char userInput[2];
    uint8_t segment[4];
    uint8_t list[16][2];
    uint8_t listSize;
    uint8_t chooseCell;
    int x;
    
    srand(time(0));


    clearArena();
    
    getEmptyCells(list, &listSize);
    chooseCell = rand() % listSize + 1;
    
    i = list[chooseCell][0];
    j = list[chooseCell][1];
    
    x = rand() % 2;
    
    if( x == 1 )
        tArena[i][j] = 0x02;
    else
        tArena[i][j] = 0x04;
        
    printOutArena();
    
    printf("\nMove? ");
    scanf("%s", &userInput);
    while(userInput[0]!='q') {
        switch(userInput[0]) {
        case 'w':
            for(i=0;i<4;i++) {
                getCol(segment, i, FWD);
                groupSegment(segment, 4);
                combineSegment(segment, 4);
                setCol(segment, i, FWD);
            }
            break;
        case 's':
            for(i=0;i<4;i++) {
                getCol(segment, i, REV);
                groupSegment(segment, 4);
                combineSegment(segment, 4);
                setCol(segment, i, REV);
            }
            break;
        case 'a':
            for(i=0;i<4;i++) {
                getRow(segment, i, FWD);
                groupSegment(segment, 4);
                combineSegment(segment, 4);
                setRow(segment, i, FWD);
            }
            break;
        case 'd':
            for(i=0;i<4;i++) {
                getRow(segment, i, REV);
                groupSegment(segment, 4);
                combineSegment(segment, 4);
                setRow(segment, i, REV);
            }
            break;
        default:
            break;
        }
               
        getEmptyCells(list, &listSize);
        chooseCell = rand() % listSize + 1;
        
        i = list[chooseCell][0];
        j = list[chooseCell][1];
        
        x = rand() % 2;
        if( x == 1 )
            tArena[i][j] = 0x02;
        else
            tArena[i][j] = 0x04;
        
        printOutArena();
        
        printf("Move %d:%d? ", x, RAND_MAX);
        scanf("%s", &userInput);
    }

    return 0;
}
