/*
    Project:    2048 
    Author:     Tomasz Walczak
    File:       logic.h
    Date:       2014-May-05
*/

#ifndef __LOGIC_H__
#define __LOGIC_H__

#include <stdint.h>

extern uint8_t ARENA_WIDTH;

extern void initLogic(uint8_t width);
extern void groupSegment(uint8_t* segment, uint8_t size);
extern void combineSegment(uint8_t* segment, uint8_t size);
extern void shiftSegment(uint8_t* segment, uint8_t size);

#endif