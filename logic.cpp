/*
    Project:    2048 
    Author:     Tomasz Walczak
    File:       logic.cpp
    Date:       2014-May-05
*/

#include "logic.h"

uint8_t ARENA_WIDTH;

void initLogic(uint8_t width) {
    ARENA_WIDTH = width;
}

void groupSegment(uint8_t* segment, uint8_t size) {
    uint8_t shiftCount = 0;
    uint8_t i = 0;
    
    while( (shiftCount < size) && (i < size) ) {
        if(!segment[i]) {
            shiftSegment(&segment[i], size-i);
            shiftCount++;
        } else {
            i++;
            shiftCount = 0;
        }
    }
}

void combineSegment(uint8_t* segment, uint8_t size) {
    uint8_t i = 0;
    for( i=0; i<(size-1); i++ ) {
        if( segment[i] == segment[i+1] ) {
            shiftSegment(&segment[i], size-i);
            segment[i] *= 2;
        }
    }
}

//----------------------------------------------------------------------// Shift array left //---------------
void shiftSegment(uint8_t* segment, uint8_t size) {
    uint8_t i;
    
    if(!size) return;                                                   // Size is invalid
    
    for( i=0; i<(size-1); i++)                                          // Shift array left
        segment[i] = segment[i+1];
        
    segment[i] = 0x00;                                                  // Insert NULL in empty spot
}