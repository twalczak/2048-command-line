/*
    Project:    2048 
    Author:     Tomasz Walczak
    File:       arena.h
    Date:       2014-May-05
*/

#ifndef __ARENA_H__
#define __ARENA_H__

#include <stdint.h>
#include <stdio.h>
#include <unistd.h>

#define FWD 0x01
#define REV 0x02

extern uint8_t tArena[4][4];

extern void clearArena( void );
extern void getRow(uint8_t* segment, uint8_t row, uint8_t dir);
extern void getCol(uint8_t* segment, uint8_t col, uint8_t dir);
extern void setRow(uint8_t* segment, uint8_t row, uint8_t dir);
extern void setCol(uint8_t* segment, uint8_t col, uint8_t dir);
extern void getEmptyCells(uint8_t list[][2], uint8_t* count);
extern void printOutArena(void);

#endif