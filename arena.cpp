/*
    Project:    2048 
    Author:     Tomasz Walczak
    File:       arena.cpp
    Date:       2014-May-05
*/

#include "arena.h"

uint8_t tArena[4][4] = 
{
    { 0xA1, 0xA2, 0xA3, 0xA4 },
    { 0xB1, 0xB2, 0xB3, 0xB4 },
    { 0xC1, 0xC2, 0xC3, 0xC4 },
    { 0xD1, 0xD2, 0xD3, 0xD4 }
};

void clearArena( void ) {
    uint8_t i,j;
    for( i=0; i<4; i++) {
        for( j=0; j<4; j++)
            tArena[i][j] = 0x00;
    }
}

void printOutArena(void) {
    uint8_t i,j;
    
    printf(" /-------------\\\n");
    
    for( i=0; i<4; i++) {
        printf(" | ");
        for( j=0; j<4; j++) {
            if(tArena[i][j])
                printf("%02X ", tArena[i][j]);
            else
                printf("   ");
            fflush(stdout);
            usleep(50000);
        }
        printf("|\n");
    }
    printf(" \\-------------/\n");
}

void getEmptyCells(uint8_t list[][2], uint8_t* count) {
    uint8_t i, j;
    
    *count = 0;
    
    for(i=0;i<4;i++) {
        for(j=0;j<4;j++) {
            if(!tArena[i][j]) {
                list[*count][0] = i;
                list[*count][1] = j;
                (*count)++;
            }
        }
    }
}

extern void getRow(uint8_t* segment, uint8_t row, uint8_t dir) {
    uint8_t i;
    
    if(dir == FWD) {
        for( i=0; i<4; i++) {
            segment[i] = tArena[row][i];
        }
    }
    else if(dir == REV) {
        for( i=0; i<4; i++) {
            segment[i] = tArena[row][3-i];
        }
    }
}

extern void getCol(uint8_t* segment, uint8_t col, uint8_t dir) {
    uint8_t i;
    
    if(dir == FWD) {
        for( i=0; i<4; i++) {
            segment[i] = tArena[i][col];
        }
    }
    else if(dir == REV) {
        for( i=0; i<4; i++) {
            segment[i] = tArena[3-i][col];
        }
    }
}

extern void setRow(uint8_t* segment, uint8_t row, uint8_t dir) {
    uint8_t i;
    
    if(dir == FWD) {
        for( i=0; i<4; i++) {
            tArena[row][i] = segment[i];
        }
    }
    else if(dir == REV) {
        for( i=0; i<4; i++) {
            tArena[row][3-i] = segment[i];
        }
    }
}

extern void setCol(uint8_t* segment, uint8_t col, uint8_t dir) {
    uint8_t i;
    
    if(dir == FWD) {
        for( i=0; i<4; i++) {
            tArena[i][col] = segment[i];
        }
    }
    else if(dir == REV) {
        for( i=0; i<4; i++) {
            tArena[3-i][col] = segment[i];
        }
    }
}